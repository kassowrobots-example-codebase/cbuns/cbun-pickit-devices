/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2023, Kassow Robots
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Kassow Robots nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#ifndef KSWX_PICKIT_DEVICES_PICKIT_CAMERA
#define KSWX_PICKIT_DEVICES_PICKIT_CAMERA

#include <pickit_devices/time_utl.h>
#include <kr2_program_api/api_v1/bundles/custom_device.h>

#include <thread>
#include <atomic>

#define SOCKET_TIMEOUT 1

#define LOOP_INTERVAL_MS 50

#define ERROR_UNEXPECTED_STATUS         -1
#define ERROR_FIND_CALIB_PLATE_FAILED   -2
#define ERROR_SAVE_SCENE_FAILED         -3
#define ERROR_PICKIT_IN_IDLE_MODE       -4
#define ERROR_PICKIT_IN_ROBOT_MODE      -5
#define ERROR_PICKIT_IN_CALIB_MODE      -6
#define ERROR_CONFIG_FAILED             -7

#define GENERIC_ROBOT_TYPE 2

// COMMANDS

#define RC_PICKIT_NO_COMMAND         -1
#define RC_PICKIT_CHECK_MODE         0
#define RC_PICKIT_FIND_CALIB_PLATE   10
#define RC_PICKIT_LOOK_FOR_OBJECTS   20
#define RC_PICKIT_NEXT_OBJECT        30
#define RC_PICKIT_CONFIGURE          40
#define RC_PICKIT_SAVE_SCENE         50

// STATUS

#define PICKIT_UNKNOWN_COMMAND           -99
#define PICKIT_RUNNING_MODE                0
#define PICKIT_IDLE_MODE                   1
#define PICKIT_CALIBRATION_MODE            2

#define PICKIT_FIND_CALIB_PLATE_OK        10
#define PICKIT_FIND_CALIB_PLATE_FAILED    11
#define PICKIT_OBJECT_FOUND               20
#define PICKIT_NO_OBJECTS                 21
#define PICKIT_NO_IMAGE_CAPTURED          22
#define PICKIT_EMPTY_ROI                  23

#define PICKIT_CONFIG_OK                  40
#define PICKIT_CONFIG_FAILED              41
#define PICKIT_SAVE_SCENE_OK              50
#define PICKIT_SAVE_SCENE_FAILED          51

#define MULT 10000.0

namespace kswx_pickit_devices {

    class PickItCamera : public kr2_bundle_api::CustomDevice {
    public:
        
        PickItCamera(boost::shared_ptr<kr2_program_api::ProgramInterface> api,
                           const boost::property_tree::ptree &xml_bundle_node);
        
        virtual ~PickItCamera();
        
        //
        //
        // Mandatory event methods
        //
        //
        
        //! Called by the CBun Sandbox process on instantiation (master).
        virtual int onCreate();
        //! Called by the CBun Sandbox process on erase (master).
        virtual int onDestroy();
        
        //! Called by the User Spawner provess on instantiation (slave).
        virtual int onBind();
        //! Called by the User Spawner provess on instantiation (slave).
        virtual int onUnbind();
        
        //! System events handling.
        void onHWReady(const kr2_signal::HWReady&);
        void onProgramTerminated(const kr2_signal::ProgramTerminated&);
        
        //
        //
        // Published custom methods, accessible from the CBun interface
        //
        //
        
        //! PickIt configure.
        virtual CBUN_PCALL configure(const kr2_program_api::Number &setup, const kr2_program_api::Number &product);
        
        //! PickIt in the PICKIT_ROBOT_MODE.
        virtual CBUN_PCALL checkRobotMode();
        //! PickIt in the PICKIT_CALIBRATION_MODE.
        virtual CBUN_PCALL checkCalibrationMode();
        
        //! Request Pickit to find objects in the current scene.
        virtual CBUN_PCALL findObjects();
        //! Request to return the next detected (valid and pickable) object in the detection grid.
        virtual CBUN_PCALL getNextObject();
        //! Fetch the results of the findObjects() method.
        virtual CBUN_PCALL getResult(kr2_program_api::Number &object_found, kr2_program_api::Number &objects_remaining);
        //! Fetch the recent object coordinates.
        virtual CBUN_PCALL getObjectPose(kr2_program_api::RobotPose &object_pose, int a_rotate_poses, int pose_mask);
        //! Fetch the recent type of detected object.
        virtual CBUN_PCALL getObjectType(kr2_program_api::Number &object_type);
        //! Fetch the recent dimensions of detected object.
        virtual CBUN_PCALL getObjectDimensions(kr2_program_api::Number &dim1, kr2_program_api::Number &dim2, kr2_program_api::Number &dim3);
        
        //! Trigger Pickit to localize the calibration plate.
        virtual CBUN_PCALL findCalibrationPlate();
        //! Request Pickit to save a snapshot with the last captured scene and the current configuration.
        virtual CBUN_PCALL saveSnapshot();
        
    protected:
        
        virtual CBUN_PCALL onActivate(const boost::property_tree::ptree &param_tree);
        virtual CBUN_PCALL onDeactivate();
        
        virtual CBUN_PCALL onMount(const boost::property_tree::ptree &param_tree) { CBUN_PCALL_RET_OK; }
        virtual CBUN_PCALL onUnmount() { CBUN_PCALL_RET_OK; }
    
    private:
        
        struct sockaddr_in serv_addr_;
        
        bool processActivationParams(const boost::property_tree::ptree &tree);
        
        // PickIt meta data structure
        struct meta_data {
            int32_t robot_type;
            int32_t interface_version;
        };
        
        // PickIt request_data structure
        struct request_data {
            int32_t actual_pose[7];
            int32_t command;
            int32_t setup;
            int32_t product;
            meta_data meta;
        };
        
        // PickIt response_data structure
        struct response_data {
            int32_t object_pose[7];
            int32_t object_age;
            int32_t object_type;
            int32_t object_dimensions[3];
            int32_t objects_remaining;
            int32_t status;
            meta_data meta;
        };
        
        int socket_fd_;
        
        // PickIt heartbeat thread instance
        std::atomic<bool> active_;
        std::atomic<bool> running_;
        std::thread thread_;
        
        // PickIt data
        bool result_excpected_;
        response_data last_response_;
        
        // Standard socket allocation and configuration.
        int connectPickIt();
        
        // Start the PickIt heartbeat
        void startClientLoop();
        // Stop the PickIt heartbeat
        void stopClientLoop();
        
        // Wrap and send the PickIt command up
        void sendCommand(int command, int32_t setup=0, int32_t product=0);
        // Receive and xtract PickIt response data
        bool recvResponse(response_data &response);
        
        
        TimeUtl timeutl;
        void addNs2Timespec(struct timespec &time, int ns);
        
    };
    
}

#endif // KSWX_PICKIT_DEVICES_PICKIT_CAMERA
