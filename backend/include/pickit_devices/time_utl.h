/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2023, Kassow Robots
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Kassow Robots nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#ifndef KSWX_PICKIT_DEVICES_TIME_UTIL
#define KSWX_PICKIT_DEVICES_TIME_UTIL

#include <string>
#include <cstdint>

#ifdef __MACH__
#include <mach/mach.h>
#include <mach/clock.h>
#include <mach/mach_time.h>
#endif

#ifdef __linux
#define NSEC_PER_SEC 1000000000ul
#define NSEC_PER_MSEC 1000000ul   //TODO: add include of appropriate header instead
#define USEC_PER_SEC 1000000ul
#endif

#define MSEC_PER_SEC 1000ul

#define USED_CLOCK CLOCK_REALTIME
#define CLOCK_NOT_INITIALIZED EFAULT



namespace kswx_pickit_devices {

    struct TimeUtl {
        
        TimeUtl();
        
        struct timespec now();
        long long diffMSecs(struct timespec tm1, struct timespec tm2);
        struct timespec addMSecs(struct timespec tms, long msecs);
        struct timespec diffTimeTs(const struct timespec& a_time1, const struct timespec& a_time2);
        int nanosleepUntil(const struct timespec& a_wakeup_time);
        
    private:
        
#ifdef __MACH__
        mach_timebase_info_data_t mach_tbase_;
        uint64_t mach_timeinit_;
#endif // __MACH__
        
    };

};

#endif // KSWX_PICKIT_DEVICES_TIME_UTL
