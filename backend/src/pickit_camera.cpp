/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2023, Kassow Robots
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Kassow Robots nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#include <pickit_devices/pickit_camera.h>
#include <kr2_program_api/internals/api/console.h>
#include <kr2_program_api/api_v1/bundles/arg_provider_xml.h>

#include <string>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>

using namespace kswx_pickit_devices;

REGISTER_CLASS(kswx_pickit_devices::PickItCamera)

PickItCamera::PickItCamera(boost::shared_ptr<kr2_program_api::ProgramInterface> a_api,
                                       const boost::property_tree::ptree &a_xml_bundle_node)
:   kr2_bundle_api::CustomDevice(a_api, a_xml_bundle_node),
active_(true),
running_(false),
result_excpected_(false)
{
    
}

PickItCamera::~PickItCamera()
{
}

int PickItCamera::onCreate()
{
    SUBSCRIBE(kr2_signal::HWReady, PickItCamera::onHWReady);    
    return 0;
}

int PickItCamera::onDestroy()
{
    onDeactivate();
    return 0;
}

int PickItCamera::onBind()
{
    SUBSCRIBE(kr2_signal::ProgramTerminated, PickItCamera::onProgramTerminated);
    
    if (!activation_tree_) {
        CLOG_ERR("[CBUN/PickIt] Activation params are not available");
        return -1;
    }
    
    if (!processActivationParams(*activation_tree_)) {
        CLOG_ERR("[CBUN/PickIt] Invalid activation params");
        return -2;
    }
    
    // open device connection
    if (connectPickIt() != 0) {
        CLOG_ERR("[CBUN/PickIt] Failed to open connection");
        return -3;
    }
    
    return 0;
}

int PickItCamera::onUnbind()
{
    active_.store(false, std::memory_order_release);
    close(socket_fd_);
    return 0;
}


void PickItCamera::onHWReady(const kr2_signal::HWReady&)
{
    if (activation_tree_) {
        kr2_program_api::CmdResult<> result = activate(*activation_tree_);
        switch (result.result_) {
            case kr2_program_api::CmdResult<>::EXCEPTION:
                PUBLISH_EXCEPTION(result.code_, result.message_)
                break;
            case kr2_program_api::CmdResult<>::ERROR:
                PUBLISH_ERROR(result.code_, result.message_)
                break;
        }
    }
}

void PickItCamera::onProgramTerminated(const kr2_signal::ProgramTerminated&)
{

}

CBUN_PCALL PickItCamera::onActivate(const boost::property_tree::ptree &a_param_tree)
{
    // process activation params
    if (!processActivationParams(a_param_tree)) {
        CBUN_PCALL_RET_ERROR(-1, "Invalid activation params");
    }
    
    // open device connection
    if (connectPickIt() != 0) {
        CBUN_PCALL_RET_ERROR(-2, "Failed to open connection");
    }
    
    startClientLoop();
    
    CBUN_PCALL_RET_OK;
}

CBUN_PCALL PickItCamera::onDeactivate()
{
    active_.store(false, std::memory_order_release);
    stopClientLoop();
    close(socket_fd_);
    
    CBUN_PCALL_RET_OK;
}

/** ---------------------------------------------------------------------------------------------------------
 *
 * This routine provides a standard socket allocation and configuration setup for the PickIt TCP/IP access.
 * PickIt IP address is read from the xml attributes being delivered along with the CBun instantiation,
 * while the port is hardcoded for this version.
 *
 * --------------------------------------------------------------------------------------------------------- */
int PickItCamera::connectPickIt()
{
    // open socket
    socket_fd_ = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (socket_fd_ < 0) {
        CLOG_ERR("Failed to create PickIt Camera instance: Unable to open socket");
        return -1;
    }
    
    // set socket non-blocking
    long arg;
    if ((arg = fcntl(socket_fd_, F_GETFL, NULL)) < 0) {
        CLOG_ERR("Failed to create PickIt Camera instance: Error fcntl(..., F_GETFL) (" << strerror(errno) << ")");
        return -1;
    }
    arg |= O_NONBLOCK;
    if( fcntl(socket_fd_, F_SETFL, arg) < 0) {
        CLOG_ERR("Failed to create PickIt Camera instance: Error fcntl(..., F_SETFL) (" << strerror(errno) << ")");
        return -1;
    }
    
    // connect (timeout supported)
    int res = connect(socket_fd_, (struct sockaddr *)&serv_addr_, sizeof(serv_addr_));
    if (res < 0) {
        if (errno == EINPROGRESS) {
            do {
                struct timeval tv;
                tv.tv_sec = SOCKET_TIMEOUT;
                tv.tv_usec = 0;
                fd_set myset;
                FD_ZERO(&myset);
                FD_SET(socket_fd_, &myset);
                res = select(socket_fd_+1, NULL, &myset, NULL, &tv);
                if (res < 0 && errno != EINTR) {
                    CLOG_ERR("Failed to create PickIt Camera instance: Error connecting (" << strerror(errno) << ")");
                    return -1;
                } else if (res > 0) {
                    int valopt;
                    socklen_t lon = sizeof(int);
                    if (getsockopt(socket_fd_, SOL_SOCKET, SO_ERROR, (void*)(&valopt), &lon) < 0) {
                        CLOG_ERR("Failed to create PickIt Camera instance: Error in getsockopt (" << strerror(valopt) << ")");
                        return -1;
                    }
                    if (valopt) {
                        CLOG_ERR("Failed to create PickIt Camera instance: Error in delayed connection (" << strerror(valopt) << ")");
                        return -1;
                    }
                    break;
                } else {
                    CLOG_ERR("Failed to create PickIt Camera instance: Timeout while establishing connection");
                    return -1;
                }
            } while (1);
        } else {
            CLOG_ERR("Failed to create PickIt Camera instance: " << strerror(errno));
            return -1;
        }
    }
    
    // set socket blocking again
    if( (arg = fcntl(socket_fd_, F_GETFL, NULL)) < 0) {
        CLOG_ERR("Failed to create PickIt Camera instance: Error fcntl(..., F_GETFL) (" << strerror(errno) << ")");
        return -1;
    }
    arg &= (~O_NONBLOCK);
    if( fcntl(socket_fd_, F_SETFL, arg) < 0) {
        CLOG_ERR("Failed to create PickIt Camera instance: Error fcntl(..., F_SETFL) (" << strerror(errno) << ")");
        return -1;
    }
    
    return 0;
}

/** ------------------------------------------------------------------------------------------
 *
 * Start the communcation thread to keep the PickIt system heartbeat (RC_PICKIT_NO_COMMAND)
 *
 * ------------------------------------------------------------------------------------------ */
void PickItCamera::startClientLoop()
{

    running_.store(true, std::memory_order_release);
    
    thread_ = std::thread([this]() {
        while (running_.load(std::memory_order_acquire)) {
            timespec update_time = timeutl.now();
            
            sendCommand(RC_PICKIT_NO_COMMAND);
            
            addNs2Timespec(update_time, LOOP_INTERVAL_MS * 1000000);
            
            timeutl.nanosleepUntil(update_time);
        }
    });
}

/** ------------------------------------------------------------------------------------------
 *
 * Stop the PickIt hearbeat
 *
 * ------------------------------------------------------------------------------------------ */
void PickItCamera::stopClientLoop()
{
    running_.store(false, std::memory_order_release);
    
    if (thread_.joinable()) {
        thread_.join();
    }
}


/** ------------------------------------------------------------------------------------------
 *
 * Wraps and send the command data to PickIt system.
 * The structure of the PickIt data is defined with the request_data C structure.
 *
 * ------------------------------------------------------------------------------------------ */
void PickItCamera::sendCommand(int a_command, int32_t a_setup, int32_t a_product)
{
    // KSW RC API realtime spin update
    api_->rc_api_->spin();
    // Fetch the robot toolflange frame coordinates (mandatory for the PickIt command)
    const kr2rc_api::Model::TF *tf = api_->rc_api_->arm_model_->read_TransformationByDuid(kr2rc_api::Model::SysId::FRAME_ROBOT_FLANGE, kr2rc_api::Model::SysId::FRAME_WORLD);
    
    if (tf == NULL) {
        CLOG_WARN("Unable to send command: Flange pose not available");
        return;
    }
    
    // Wrap and send the PickIt command
    request_data request;
    request.actual_pose[0] = htonl((int32_t) (tf->pose_.p_.x() * MULT));
    request.actual_pose[1] = htonl((int32_t) (tf->pose_.p_.y() * MULT));
    request.actual_pose[2] = htonl((int32_t) (tf->pose_.p_.z() * MULT));
    double quat_x, quat_y, quat_z, quat_w;
    tf->pose_.M_.getQuaternion(quat_x, quat_y, quat_z, quat_w);
    request.actual_pose[3] = htonl((int32_t) (quat_w * MULT));
    request.actual_pose[4] = htonl((int32_t) (quat_x * MULT));
    request.actual_pose[5] = htonl((int32_t) (quat_y * MULT));
    request.actual_pose[6] = htonl((int32_t) (quat_z * MULT));
    request.command = htonl(a_command);
    request.setup = htonl(a_setup);
    request.product = htonl(a_product);
    request.meta.robot_type = htonl(GENERIC_ROBOT_TYPE);
    request.meta.interface_version = htonl(11);
    
    // Write the request into socket
    if (write(socket_fd_, (void*)&request, sizeof(request)) != sizeof(request)) {
        CLOG_WARN("Unable to send command: Incorrect number of bytes have been written");
    }
}

/* ------------------------------------------------------------------------------------------
 *
 * Receive the PickIt response into response_data structure.
 *
 * ------------------------------------------------------------------------------------------ */
bool PickItCamera::recvResponse(response_data &a_response)
{
    int available;
    do {
        ioctl(socket_fd_, FIONREAD, &available);
        if (available >= sizeof(a_response)) {
            break;
        }
    } while (active_);
    
    return read(socket_fd_, &a_response, sizeof(a_response)) == sizeof(a_response);
}

void PickItCamera::addNs2Timespec(struct timespec &a_time, int a_ns)
{
    a_time.tv_sec += a_ns / ((long long int) NSEC_PER_SEC);
    a_time.tv_nsec += a_ns % ((long long int) NSEC_PER_SEC);
    if (a_time.tv_nsec >= (long long int) NSEC_PER_SEC)
    {
        a_time.tv_nsec -= (long long int)  NSEC_PER_SEC;
        a_time.tv_sec += 1;
    }
    if (a_time.tv_nsec < 0)
    {
        a_time.tv_nsec += (long long int)  NSEC_PER_SEC;
        a_time.tv_sec -= 1;
    }
}

/** ------------------------------------------------------------------------------------------
 *
 * CBUN Published method
 *
 * Implementing RC_PICKIT_CONFIGURE
 * (http://docs.pickit3d.com/docs/pickit/en/2.2/robot-integrations/socket/)
 *
 * Request Pickit to load a specific setup and product configuration.
 * Each setup and product configuration have a unique ID assigned, which is shown in the web interface, next to the configuration name.
 *
 * ------------------------------------------------------------------------------------------ */
CBUN_PCALL PickItCamera::configure(const kr2_program_api::Number &a_setup, const kr2_program_api::Number &a_product)
{
    sendCommand(RC_PICKIT_CONFIGURE, (int32_t) a_setup.l(), (int32_t) a_product.l());
    
    response_data response;
    recvResponse(response);
    
    int32_t status = ntohl(response.status);
    if (status == PICKIT_CONFIG_OK) {
        CBUN_PCALL_RET_OK;
    } else if (status == PICKIT_CONFIG_FAILED) {
        CBUN_PCALL_RET_ERROR(ERROR_CONFIG_FAILED, "Failed to configure pickit");
    } else {
        CBUN_PCALL_RET_ERROR(ERROR_UNEXPECTED_STATUS, "Unexpected status");
    }
}

/** ------------------------------------------------------------------------------------------
 *
 * CBUN Published method
 *
 * Implementing RC_PICKIT_CHECK_MODE
 * (http://docs.pickit3d.com/docs/pickit/en/2.2/robot-integrations/socket)
 *
 * Check the current mode of Pickit is in PICKIT_ROBOT_MODE.
 * If true return CBUN_PCALL_RET_OK.
 * ------------------------------------------------------------------------------------------ */
CBUN_PCALL PickItCamera::checkRobotMode()
{
    sendCommand(RC_PICKIT_CHECK_MODE);
    
    response_data response;
    recvResponse(response);
    
    int32_t status = ntohl(response.status);
    if (status == PICKIT_RUNNING_MODE) {
        CBUN_PCALL_RET_OK;
    } else if (status == PICKIT_IDLE_MODE) {
        CBUN_PCALL_RET_ERROR(ERROR_PICKIT_IN_IDLE_MODE, "Pickit in idle mode");
    } else if (status == PICKIT_CALIBRATION_MODE) {
        CBUN_PCALL_RET_ERROR(ERROR_PICKIT_IN_CALIB_MODE, "Pickit in calibration mode");
    } else {
        CBUN_PCALL_RET_ERROR(ERROR_UNEXPECTED_STATUS, "Unexpected status");
    }
}

/** ------------------------------------------------------------------------------------------
 *
 * CBUN Published method
 *
 * Implementing RC_PICKIT_CHECK_MODE
 * (http://docs.pickit3d.com/docs/pickit/en/2.2/robot-integrations/socket/)
 *
 * Check the current mode of Pickit is in PICKIT_CALIBRATION_MODE.
 * If true return CBUN_PCALL_RET_OK.
 * ------------------------------------------------------------------------------------------ */
CBUN_PCALL PickItCamera::checkCalibrationMode()
{
    sendCommand(RC_PICKIT_CHECK_MODE);
    
    response_data response;
    recvResponse(response);
    
    int32_t status = ntohl(response.status);
    if (status == PICKIT_CALIBRATION_MODE) {
        CBUN_PCALL_RET_OK;
    } else if (status == PICKIT_IDLE_MODE) {
        CBUN_PCALL_RET_ERROR(ERROR_PICKIT_IN_IDLE_MODE, "Pickit in idle mode");
    } else if (status == PICKIT_RUNNING_MODE) {
        CBUN_PCALL_RET_ERROR(ERROR_PICKIT_IN_ROBOT_MODE, "Pickit in robot mode");
    } else {
        CBUN_PCALL_RET_ERROR(ERROR_UNEXPECTED_STATUS, "Unexpected status");
    }
}

/** ------------------------------------------------------------------------------------------
 *
 * CBUN Published method
 *
 * Implementing RC_PICKIT_LOOK_FOR_OBJECTS
 * (http://docs.pickit3d.com/docs/pickit/en/2.2/robot-integrations/socket/)
 *
 * Request Pickit to find objects in the current scene.
 * This command performs an image capture and image processing in a single request.
 * ------------------------------------------------------------------------------------------ */
CBUN_PCALL PickItCamera::findObjects()
{
    sendCommand(RC_PICKIT_LOOK_FOR_OBJECTS);
    result_excpected_ = true;
    CBUN_PCALL_RET_OK;
}

/** ------------------------------------------------------------------------------------------
 *
 * CBUN Published method
 *
 * Implementing RC_PICKIT_NEXT_OBJECT
 * (http://docs.pickit3d.com/docs/pickit/en/2.2/robot-integrations/socket/)
 *
 * Request to return the next detected (valid and pickable) object in the detection grid.
 * Use this command when a single object detection run yields multiple detected objects.
 * ------------------------------------------------------------------------------------------ */
CBUN_PCALL PickItCamera::getNextObject()
{
    sendCommand(RC_PICKIT_NEXT_OBJECT);
    result_excpected_ = true;
    CBUN_PCALL_RET_OK;
}

/** ------------------------------------------------------------------------------------------
 *
 * CBUN Published method
 *
 * Fetch the results of the findObjects() method.
 * a_object_found stores the true/false value if the PickIt object was even detected.
 * a_objects_remaining holds the actual number of objects in the array.
 * ------------------------------------------------------------------------------------------ */
CBUN_PCALL PickItCamera::getResult(kr2_program_api::Number &a_object_found, kr2_program_api::Number &a_objects_remaining)
{
    if (result_excpected_) {
        result_excpected_ = false;
    
        recvResponse(last_response_);
        
        int32_t status = ntohl(last_response_.status);
        long objects_remaining = ntohl(last_response_.objects_remaining);
        if (status == PICKIT_OBJECT_FOUND) {
            a_object_found = (long) 1;
        } else if (status == PICKIT_NO_OBJECTS || status == PICKIT_EMPTY_ROI) {
            a_object_found = (long) 0;
        } else {
            CBUN_PCALL_RET_ERROR(ERROR_UNEXPECTED_STATUS, "Unexpected status");
        }
        
        a_objects_remaining = objects_remaining;
    } else {
        a_object_found = (long) 0;
        a_objects_remaining = (long) 0;
    }
    
    CBUN_PCALL_RET_OK;
}

/** ------------------------------------------------------------------------------------------
 *
 * CBUN Published method
 *
 * Fetch the recent object coordinates.
 * a_pose_mask is used to eliminate position or orientation coordinates from the returned data.
 * ------------------------------------------------------------------------------------------ */
CBUN_PCALL PickItCamera::getObjectPose(kr2_program_api::RobotPose &a_object_pose, int a_rotate_poses, int a_pose_mask)
{
    double r, p, y;

    if (a_pose_mask & 1) {
        double pos_x = ((int32_t) ntohl(last_response_.object_pose[0]))/MULT;
        double pos_y = ((int32_t) ntohl(last_response_.object_pose[1]))/MULT;
        double pos_z = ((int32_t) ntohl(last_response_.object_pose[2]))/MULT;
        a_object_pose.pos() = kr2_program_api::Position(pos_x, pos_y, pos_z);
    }
    
    if (a_pose_mask & 2) {
        double rot_w = ((int32_t) ntohl(last_response_.object_pose[3]))/MULT;
        double rot_x = ((int32_t) ntohl(last_response_.object_pose[4]))/MULT;
        double rot_y = ((int32_t) ntohl(last_response_.object_pose[5]))/MULT;
        double rot_z = ((int32_t) ntohl(last_response_.object_pose[6]))/MULT;

        a_object_pose.rot() = kr2_program_api::Rotation::Quaternion(rot_x, rot_y, rot_z, rot_w);

        if (a_rotate_poses == 1) {
            a_object_pose.rot().getRPY(r, p, y);
            a_object_pose.rot() = kr2_program_api::Rotation::RPY(r + M_PI, p, y);
        }
    }
    
    CBUN_PCALL_RET_OK;
}


/** ------------------------------------------------------------------------------------------
 *
 * CBUN Published method
 *
 * Fetch the recent type of detected object.
 * a_object_type PICKIT_TYPE_* values.
 * ------------------------------------------------------------------------------------------ */
CBUN_PCALL PickItCamera::getObjectType(kr2_program_api::Number &a_object_type)
{
    a_object_type = (long) ntohl(last_response_.object_type);
    
    CBUN_PCALL_RET_OK;
}

/** ------------------------------------------------------------------------------------------
 *
 * CBUN Published method
 *
 * Fetch the recent dimensions of detected object.
 * a_dim1: object length (SQUARE, RECTANGLE, ELLIPSE, CYLINDER, POINTCLOUD, BLOB) or diameter (CIRCLE, SPHERE) in meters.
 * a_dim2: object width (RECTANGLE, ELLIPSE, POINTCLOUD, BLOB) or diameter (CYLINDER) in meters.
 * a_dim3: object height (POINTCLOUD, BLOB) in meters.
 * ------------------------------------------------------------------------------------------ */
CBUN_PCALL PickItCamera::getObjectDimensions(kr2_program_api::Number &a_dim1, kr2_program_api::Number &a_dim2, kr2_program_api::Number &a_dim3)
{
    a_dim1 = (double) ((int32_t) ntohl(last_response_.object_dimensions[0]))/MULT;
    a_dim2 = (double) ((int32_t) ntohl(last_response_.object_dimensions[1]))/MULT;
    a_dim3 = (double) ((int32_t) ntohl(last_response_.object_dimensions[2]))/MULT;
    
    CBUN_PCALL_RET_OK;
}

/** ------------------------------------------------------------------------------------------
 *
 * CBUN Published method
 *
 * Implementing RC_PICKIT_FIND_CALIB_PLATE
 * (http://docs.pickit3d.com/docs/pickit/en/2.2/robot-integrations/socket/)
 *
 * Trigger Pickit to localize the calibration plate.
 * If sufficient calibration poses have been collected, Pickit will additionally trigger the computation of the robot-camera calibration.
 * Note that Pickit has to be in calibration mode when this command is sent.
 * ------------------------------------------------------------------------------------------ */
CBUN_PCALL PickItCamera::findCalibrationPlate()
{
    sendCommand(RC_PICKIT_FIND_CALIB_PLATE);
    
    response_data response;
    recvResponse(response);
    
    int32_t status = ntohl(response.status);
    if (status == PICKIT_FIND_CALIB_PLATE_OK) {
        CBUN_PCALL_RET_OK;
    } else if (status == PICKIT_FIND_CALIB_PLATE_FAILED) {
        CBUN_PCALL_RET_ERROR(ERROR_FIND_CALIB_PLATE_FAILED, "Failed to find calib plate");
    } else {
        CBUN_PCALL_RET_ERROR(ERROR_UNEXPECTED_STATUS, "Unexpected status");
    }
}

/** ------------------------------------------------------------------------------------------
 *
 * CBUN Published method
 *
 * Implementing RC_PICKIT_SAVE_SNAPSHOT
 * (http://docs.pickit3d.com/docs/pickit/en/2.2/robot-integrations/socket/)
 *
 * Request Pickit to save a snapshot with the last captured scene and the current configuration.
 * Snapshots will be saved in the robot subfolder, which can be accessed from the web interface.
 * ------------------------------------------------------------------------------------------ */
CBUN_PCALL PickItCamera::saveSnapshot()
{
    sendCommand(RC_PICKIT_SAVE_SCENE);
    
    response_data response;
    recvResponse(response);
    
    int32_t status = ntohl(response.status);
    if (status == PICKIT_SAVE_SCENE_OK) {
        CBUN_PCALL_RET_OK;
    } else if (status == PICKIT_SAVE_SCENE_FAILED) {
        CBUN_PCALL_RET_ERROR(ERROR_SAVE_SCENE_FAILED, "Failed to save snapshot");
    } else {
        CBUN_PCALL_RET_ERROR(ERROR_UNEXPECTED_STATUS, "Unexpected status");
    }
}

bool PickItCamera::processActivationParams(const boost::property_tree::ptree &a_param_tree)
{
    kr2_bundle_api::ArgProviderXml arg_provider(a_param_tree);
    if (arg_provider.getArgCount() != 1) {
        CLOG_ERR("[CBUN/PickIt] Unexpected param count=" << arg_provider.getArgCount());
        return false;
    }
    
    std::string ip_address = arg_provider.getString(0);
    
    serv_addr_.sin_family = AF_INET;
    serv_addr_.sin_port = htons(5001);
    
    // process pickit ip address
    if (inet_pton(AF_INET, ip_address.c_str(), &serv_addr_.sin_addr)<=0) {
        CLOG_ERR("Failed to create PickIt Camera instance: IP Address not supported");
        return -1;
    }
    
    return true;
}
