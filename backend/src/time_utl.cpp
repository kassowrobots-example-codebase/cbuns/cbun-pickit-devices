/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2023, Kassow Robots
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Kassow Robots nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#include <pickit_devices/time_utl.h>

#include <time.h>
#include <sys/time.h>

using namespace kswx_pickit_devices;


TimeUtl::TimeUtl() {
#ifdef __MACH__
    mach_timebase_info(&mach_tbase_);
#endif // __MACH__
}

struct timespec TimeUtl::now()
{
    struct timespec ret;
#ifdef __MACH__
    uint64_t d = mach_absolute_time() - mach_timeinit_;
    d *= mach_tbase_.numer;
    d /= mach_tbase_.denom;
    
    ret.tv_sec = d / NSEC_PER_SEC;
    ret.tv_nsec = d % NSEC_PER_SEC;
#else
    clock_gettime(USED_CLOCK, &ret);
#endif
    return ret;
}

long long TimeUtl::diffMSecs(struct timespec tm1, struct timespec tm2) {
    return (tm1.tv_sec == tm2.tv_sec ? (tm2.tv_nsec - tm1.tv_nsec)/NSEC_PER_MSEC : ((tm2.tv_sec - tm1.tv_sec - 1)*MSEC_PER_SEC + MSEC_PER_SEC - tm1.tv_nsec/NSEC_PER_MSEC + tm2.tv_nsec/NSEC_PER_MSEC));
}
        
struct timespec TimeUtl::addMSecs(struct timespec tms, long msecs) {
    tms.tv_sec += (tms.tv_nsec + msecs * NSEC_PER_MSEC)/NSEC_PER_SEC;
    tms.tv_nsec = (tms.tv_nsec + msecs * NSEC_PER_MSEC)%NSEC_PER_SEC;
    return tms;
}

struct timespec TimeUtl::diffTimeTs(const struct timespec& a_time1, const struct timespec& a_time2) {
    struct timespec res, time1, time2;
    
    time1 = a_time1;
    time2 = a_time2;
    
    /* Perform the carry for the later subtraction by updating y. */
    if (time1.tv_nsec < time2.tv_nsec) {
        unsigned long nsec = (time2.tv_nsec - time1.tv_nsec) / NSEC_PER_SEC + 1;
        time2.tv_nsec -= NSEC_PER_SEC * nsec;
        time2.tv_sec += nsec;
    }
    
    if (time1.tv_nsec > time2.tv_nsec + (signed long)(NSEC_PER_SEC)) {
        unsigned long nsec = (time1.tv_nsec - time2.tv_nsec) / NSEC_PER_SEC;
        time2.tv_nsec += NSEC_PER_SEC * nsec;
        time2.tv_sec -= nsec;
    }
    
    /* Compute the time remaining to wait.
     tv_usec is certainly positive. */
    res.tv_sec = time1.tv_sec - time2.tv_sec;
    res.tv_nsec = time1.tv_nsec - time2.tv_nsec;
    
    return res;
}
        
int TimeUtl::nanosleepUntil(const struct timespec& a_wakeup_time) {
    struct timespec relative_sleep_time, ts_now;
#ifdef __MACH__
    ts_now = now(); //clock_gettime(USED_CLOCK, &now);
    relative_sleep_time = diffTimeTs(a_wakeup_time, ts_now);
    return nanosleep(&relative_sleep_time, NULL);
#else
    clock_gettime(USED_CLOCK, &ts_now);
    relative_sleep_time = diffTimeTs(a_wakeup_time, ts_now);
    return clock_nanosleep(USED_CLOCK, 0, &relative_sleep_time, NULL);
#endif
}

